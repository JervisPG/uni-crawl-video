package upload

import (
	"github.com/gogf/gf/v2/net/ghttp"
	"github.com/gogf/gf/v2/os/gctx"
	"uni-crawl-frame/db/mysql/dao"
	"uni-crawl-frame/db/mysql/model/entity"
	"uni-crawl-frame/entity/crawldto"
	"uni-crawl-frame/utils/httputil"
)

var QryExe = new(qryExe)

type qryExe struct{}

// 查询任务列表
func (c *qryExe) ListAllTask(r *ghttp.Request) {

	qry := new(crawldto.CmsUploadQueueQry)
	httputil.ParsePageParam(r, qry)

	qryDao := dao.CmsUploadQueue
	if qry.Id != 0 {
		var queue *entity.CmsUploadQueue
		_ = qryDao.Ctx(gctx.GetInitCtx()).Where(qryDao.Columns().Id, qry.Id).Scan(&queue)
		httputil.SuccessData(r, queue)
	}

	if qry.UploadStatus != 0 {
		qryDao.Ctx(gctx.GetInitCtx()).Where(qryDao.Columns().UploadStatus, qry.UploadStatus)
	}

	list, _ := qryDao.Ctx(gctx.GetInitCtx()).Page(qry.PageIndex, qry.PageSize).All()
	httputil.SuccessData(r, list)

}
