package controller

import (
	"fmt"
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/net/ghttp"
	"github.com/gogf/gf/v2/os/gfile"
	"uni-crawl-frame/service/configservice"
	"uni-crawl-video/controller/upload"
)

func init() {
	server := g.Server()
	// 跨域处理
	//server.Use(middleware.CORS)
	//// 登录验证中间件
	//server.Use(middleware.CheckLogin)
	//// 操作日志中间件
	//server.Use(middleware.OperLog)
	//// 登录日志中间件
	//server.Use(middleware.LoginLog)

	replaytask := configservice.GetString("server.openReplayTaskList")
	//g.Dump(replaytask)
	if len(g.NewVar(replaytask).Array()) > 0 {
		//replayPath := fmt.Sprintf("%s/replay", rootPath)
		//_ = gfile.Mkdir(replayPath)
		//server.SetServerRoot(replayPath)
	}
	rootPath := configservice.GetString("dfs.rootPath")
	replayPath := fmt.Sprintf("%s/live", rootPath)
	_ = gfile.Mkdir(replayPath)
	server.SetServerRoot(replayPath)
	/* 根路径 */
	contextPath := configservice.GetString("server.context-path")
	server.Group(contextPath, func(grp *ghttp.RouterGroup) {
		//initEasyGoRouter(grp)

		// 视频上传队列
		grp.Group("/upload", func(group *ghttp.RouterGroup) {
			group.POST("/createTask", upload.CmdExe.CreateTask)
			group.POST("/updateTaskStatus", upload.CmdExe.UpdateTaskStatus)
			group.POST("/listAllTask", upload.QryExe.ListAllTask)
		})

	})

}
