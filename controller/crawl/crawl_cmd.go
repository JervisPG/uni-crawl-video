package crawl

import (
	"github.com/gogf/gf/v2/net/ghttp"
	"github.com/gogf/gf/v2/os/gctx"
	"github.com/gogf/gf/v2/util/gconv"
	"uni-crawl-frame/core"
	"uni-crawl-frame/db/mysql/dao"
	"uni-crawl-frame/db/mysql/model/entity"
	"uni-crawl-frame/entity/crawldto"
	"uni-crawl-frame/service/crawl/vodservice"
	"uni-crawl-frame/utils/httputil"
)

var CmdExe = new(cmdExe)

type cmdExe struct{}

// 创建抓取任务
func (c *cmdExe) CreateTask(r *ghttp.Request) {

	create := new(crawldto.CmsCrawlQueueCreate)
	httputil.ParseParam(r, create)
	queue := new(entity.CmsCrawlQueue)
	gconv.Struct(create, queue)
	if create.CrawlType == vodservice.TypePageUrl {
		if create.CrawlSeedUrl == "" {
			httputil.Error(r, "页面URL为空")
		}
	} else if create.CrawlType == vodservice.TypeM3U8Url {
		if create.CrawlM3U8Url == "" {
			httputil.Error(r, "M3U8 URL为空")
		}
		queue.CrawlStatus = vodservice.Init
	} else if create.CrawlType == vodservice.TypeMP4Url {
		if create.CrawlM3U8Url == "" {
			httputil.Error(r, "MP4 URL为空")
		}
		queue.CrawlStatus = vodservice.CrawlFinish
	}

	queue.HostType = core.StrategySelector.GetHostType(queue.CrawlSeedUrl)
	dao.CmsCrawlQueue.Ctx(gctx.GetInitCtx()).Insert(queue)
	httputil.Success(r)

}
