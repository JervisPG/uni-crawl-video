package crawl

import (
	"github.com/gogf/gf/v2/net/ghttp"
	"github.com/gogf/gf/v2/os/gctx"
	"github.com/gogf/gf/v2/util/gconv"
	"uni-crawl-frame/db/mysql/dao"
	"uni-crawl-frame/db/mysql/model/entity"
	"uni-crawl-frame/entity/crawldto"
	"uni-crawl-frame/service/crawl/videoservice"
	"uni-crawl-frame/utils/httputil"
)

var (
	QryExe        = new(qryExe)
	crawlColumns  = dao.CmsCrawlQueue.Columns()
	uploadColumns = dao.CmsUploadQueue.Columns()
)

type qryExe struct{}

// 查询任务
func (c *qryExe) GetOne(r *ghttp.Request) {

	qry := new(crawldto.CmsCrawlQueueQry)
	httputil.ParseParam(r, qry)

	var seed *entity.CmsCrawlQueue
	_ = dao.CmsCrawlQueue.Ctx(gctx.GetInitCtx()).Where(crawlColumns.VideoItemId, qry.VideoItemId).Scan(&seed)
	if seed == nil {
		httputil.Error(r, "不存在抓取任务")
	}

	crawlDTO := new(crawldto.CmsCrawl)
	gconv.Struct(seed, crawlDTO)
	crawlDTO.ShowStatus = seed.CrawlStatus
	crawlDTO.ResourcePath = videoservice.GetVideoDir(seed.CountryCode, seed.VideoYear, seed.VideoCollId, seed.VideoItemId)

	var uploadQueue *entity.CmsUploadQueue
	_ = dao.CmsUploadQueue.Ctx(gctx.GetInitCtx()).Where(uploadColumns.VideoItemId, qry.VideoItemId).Scan(&uploadQueue)
	if uploadQueue != nil {
		// 状态结合
		crawlDTO.ShowStatus += uploadQueue.UploadStatus
	}

	httputil.SuccessData(r, crawlDTO)

}
