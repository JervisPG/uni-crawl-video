package programme

import (
	"testing"
	"uni-crawl-frame/db/mysql/model/entity"
)

func TestRequestProgramme(t *testing.T) {
	config := new(entity.CmsCrawlReplayConfig)
	config.SeedUrl = "https://programme.tvb.com/jade"
	config.Id = 2
	programmeCrawl := new(ProgrammeCrawl)
	m := &entity.CmsCrawlReplayManifestTask{
		Id: 1,
	}
	programmeCrawl.CreateProgram(config, m)
}
