package sports8

import (
	"fmt"
	"github.com/gocolly/colly"
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/os/gctx"
	"github.com/gogf/gf/v2/os/gtime"
	"github.com/gogf/gf/v2/text/gregex"
	"time"
	"uni-crawl-frame/core"
	"uni-crawl-frame/db/mysql/model/entity"
	"uni-crawl-frame/service/crawl/replayservice"
	"uni-crawl-frame/utils/timeutil"
)

type Sports8Crawl struct {
	*core.AbstractCrawlReplayUrl
}

// CreateProgram eg: https://sports8.net/program/70/2.htm
func (receiver *Sports8Crawl) CreateProgram(replayConfig *entity.CmsCrawlReplayConfig, manifestTask *entity.CmsCrawlReplayManifestTask) {
	log := g.Log().Line()
	programTitle := ""

	now := time.Now()
	todayStr := now.Format(timeutil.YYYY_MM_DD)

	coll := colly.NewCollector()

	coll.OnXML("//*[@id='Weepgprogram_program_yugo']//*[@class='live-link'][1]", func(element *colly.XMLElement) {
		programTitle = element.Text
		log.Infof(gctx.GetInitCtx(),
			"sports8电视台名称 = %s", programTitle)
	})

	var programTasks []*entity.CmsCrawlReplayProgramTask

	pLen := 0
	coll.OnXML("//*[@id='Weepgprogram_epgInfo']//*[contains(@class,'pc_')]", func(element *colly.XMLElement) {

		timeDurationStr := element.Text // eg: 00:30活力影院:渡江侦察记
		timeAndTitle, _ := gregex.MatchString("(\\d{2}):(\\d{2})(.*)", timeDurationStr)
		lHourStr := timeAndTitle[1]
		lMinuteStr := timeAndTitle[2]
		title := timeAndTitle[3]
		log.Infof(gctx.GetInitCtx(),
			"节目 = %s. start at %s %s:%s", title, todayStr, lHourStr, lMinuteStr)

		programTask := new(entity.CmsCrawlReplayProgramTask)
		programTask.ManifestId = manifestTask.Id
		programTask.ProgramName = title
		programTask.ConfigId = replayConfig.Id
		programTask.CrawlStatus = replayservice.ProgramTaskInit

		programStartTime, _ := time.Parse(timeutil.YYYY_MM_DD_HH_MM_SS, fmt.Sprintf("%s %s:%s:00", todayStr, lHourStr, lMinuteStr))
		programTask.ProgramStartTime = gtime.NewFromTime(programStartTime)
		programTask.ProgramNo = programTask.ProgramStartTime.TimestampMilliStr()
		programTask.HostIp = replayConfig.Host
		programTask.CreateTime = gtime.Now()

		programTasks = append(programTasks, programTask)
		pLen = len(programTasks)
	})

	coll.OnScraped(func(response *colly.Response) {
		for i, programTask := range programTasks {

			var programNextStarTime *gtime.Time
			if i+1 == pLen {
				// 最后一个节目单无法计算时长，只能手动赋予一个
				programNextStarTime = programTask.ProgramStartTime.Add(time.Hour)
			} else {
				// 下一个节目单的开始录制时间作为当前节目的结束录制时间
				programNextStarTime = programTasks[i+1].ProgramStartTime
			}

			sec := (programNextStarTime.TimestampMilli() - programTask.ProgramStartTime.TimestampMilli()) / 1000
			programTask.ProgramEndTime = programTask.ProgramStartTime.Add(gtime.S * time.Duration(sec))

			replayservice.CheckAndSave(programTask)
		}
	})

	_ = coll.Visit(replayConfig.SeedUrl)

}
