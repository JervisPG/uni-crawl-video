package programtable

import (
	"fmt"
	"github.com/gocolly/colly"
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/os/gctx"
	"github.com/gogf/gf/v2/os/gtime"
	"github.com/gogf/gf/v2/text/gregex"
	"time"
	"uni-crawl-frame/core"
	"uni-crawl-frame/db/mysql/model/entity"
	"uni-crawl-frame/service/crawl/replayservice"
	"uni-crawl-frame/utils/timeutil"
)

type ProgramtableCrawl struct {
	*core.AbstractCrawlReplayUrl
}

// eg : https://節目表.tw/channel/台視/#
func (receiver *ProgramtableCrawl) CreateProgram(replayConfig *entity.CmsCrawlReplayConfig, manifestTask *entity.CmsCrawlReplayManifestTask) {
	log := g.Log().Line()
	programTitle := ""

	now := time.Now()
	todayStr := now.Format(timeutil.YYYY_MM_DD)

	coll := colly.NewCollector()

	coll.OnXML("//*[@class='inline-block align-top']", func(element *colly.XMLElement) {
		programTitle = element.Text
		log.Infof(gctx.GetInitCtx(), "節目表.tw电视台名称 = %s", programTitle)
	})

	var programTasks []*entity.CmsCrawlReplayProgramTask

	pLen := 0
	//coll.OnXML("//*[@class='date ng-scope'][1]//*[@class='p ng-scope']", func(element *colly.XMLElement) {
	coll.OnXML("//*[@class='ps'][4]/*[@class='p']", func(element *colly.XMLElement) {

		startTimeStr := element.ChildText("/*[@class='time']") // eg: 00:30
		matchResults, _ := gregex.MatchString("(\\d{2}):(\\d{2})", startTimeStr)
		lHourStr := matchResults[1]
		lMinuteStr := matchResults[2]
		title := element.ChildText("/*[@class='name']")
		log.Infof(gctx.GetInitCtx(),
			"节目 = %s. start at %s %s:%s", title, todayStr, lHourStr, lMinuteStr)

		programTask := new(entity.CmsCrawlReplayProgramTask)
		programTask.ManifestId = manifestTask.Id
		programTask.ConfigId = replayConfig.Id
		programTask.ProgramName = title
		programTask.CrawlStatus = replayservice.ProgramTaskInit

		programStartTime, _ := time.Parse(timeutil.YYYY_MM_DD_HH_MM_SS, fmt.Sprintf("%s %s:%s:00", todayStr, lHourStr, lMinuteStr))
		programTask.ProgramStartTime = gtime.NewFromTime(programStartTime)
		programTask.ProgramNo = programTask.ProgramStartTime.TimestampMilliStr()
		programTask.HostIp = replayConfig.Host
		programTask.CreateTime = gtime.Now()

		programTasks = append(programTasks, programTask)
		pLen = len(programTasks)
	})

	coll.OnScraped(func(response *colly.Response) {
		for i, programTask := range programTasks {

			var programNextStarTime *gtime.Time
			if i+1 == pLen {
				// 最后一个节目单无法计算时长，只能手动赋予一个
				programNextStarTime = programTask.ProgramStartTime.Add(time.Hour)
			} else {
				// 下一个节目单的开始录制时间作为当前节目的结束录制时间
				programNextStarTime = programTasks[i+1].ProgramStartTime
			}

			sec := (programNextStarTime.TimestampMilli() - programTask.ProgramStartTime.TimestampMilli()) / 1000
			programTask.ProgramEndTime = programTask.ProgramStartTime.Add(gtime.S * time.Duration(sec))
			programTask.ConfigId = replayConfig.Id
			replayservice.CheckAndSave(programTask)
		}
	})

	_ = coll.Visit(replayConfig.SeedUrl)

}
