package autolist

import (
	"fmt"
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/os/gtime"
	"time"
	"uni-crawl-frame/core"
	"uni-crawl-frame/db/mysql/model/entity"
	"uni-crawl-frame/service/configservice"
	"uni-crawl-frame/service/crawl/replayservice"
)

type AutoList struct {
	*core.AbstractCrawlReplayUrl
}

func (receiver *AutoList) CreateProgram(replayConfig *entity.CmsCrawlReplayConfig, manifestTask *entity.CmsCrawlReplayManifestTask) {
	//g.Dump(replayConfig)
	var create_num int = 24
	startTimeStr := gtime.Now().Format("Y-m-d 00:00:00")
	firstTime := gtime.New(startTimeStr)
	auto := configservice.GetBool("replay.aotulist")
	for i := 0; i < create_num; i++ {
		sTime := firstTime.Add(time.Hour * g.NewVar(i).Duration())
		eTime := firstTime.Add(time.Hour * g.NewVar(i+1).Duration())
		//g.Log().Infof(gctx.GetInitCtx(), "index:%v---sTime:%v--eTime:%v",i,sTime,eTime)
		programTask := new(entity.CmsCrawlReplayProgramTask)
		programTask.ManifestId = manifestTask.Id
		programTask.ConfigId = replayConfig.Id
		programTask.ProgramNo = g.NewVar(sTime.Unix()).String()
		if auto {
			programTask.ProgramName = g.NewVar(sTime).String()
		} else {
			programTask.ProgramName = fmt.Sprintf("节目号:%d", i+1)
		}

		programTask.ProgramStartTime = sTime
		programTask.ProgramEndTime = eTime
		programTask.CreateTime = gtime.Now()
		programTask.CrawlStatus = replayservice.ProgramTaskInit
		programTask.HostIp = replayConfig.Host
		replayservice.CheckAndSave(programTask)
	}

}
