package task

import (
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/os/gcron"
	"time"
	"uni-crawl-frame/service/configservice"
	"uni-crawl-frame/service/crawl/vodservice"
	"uni-crawl-frame/task"
	"uni-crawl-frame/task/crawltask"
	"uni-crawl-frame/task/livetask"
	"uni-crawl-frame/task/replay"
	"uni-crawl-frame/task/taskfactory"
	"uni-crawl-frame/task/xxljob"
	videoCrawlTask "uni-crawl-video/task/crawltask"
)

func StartAll() {
	log := g.Log().Line()

	// 根据config.yml配置开启所有Task
	// 点播
	vodTaskNameList := configservice.GetStrings("server.openVodTaskList")
	taskfactory.DoStartAllTask(log, vodTaskNameList)

	// 回看
	replayTaskNameList := configservice.GetStrings("server.openReplayTaskList")
	taskfactory.DoStartAllTask(log, replayTaskNameList)

	// 直播
	liveTaskNameList := []string{task.CrawlLiveTask}
	taskfactory.DoStartAllTask(log, liveTaskNameList)
}

func RegistryAllDefaultVideoTask(jobWrapper *xxljob.XxlJobWrapper) {
	jobWrapper.RegTask("crawlUrlType1Task", crawltask.CrawlTask.CrawlUrlType1Task)
	jobWrapper.RegTask("crawlUrlType2Task", crawltask.CrawlTask.CrawlUrlType2Task)
	jobWrapper.RegTask("crawlUrlType3Task", crawltask.CrawlTask.CrawlUrlType3Task)
	jobWrapper.RegTask("downloadMp4Type1Task", crawltask.DownloadMp4Type1Task)
	jobWrapper.RegTask("downloadMp4Type2Task", crawltask.DownloadMp4Type2Task)
	jobWrapper.RegTask("downloadMp4Type3Task", crawltask.DownloadMp4Type3Task)

	// 【点播】定时生成视频列表抓取任务实例，根据实例去真正启动视频列表抓取任务
	jobWrapper.RegTask("genVodConfigTask", crawltask.CrawlVodTVTask.GenVodConfigTask)
	// 【点播】根据点播配置和支持的策略自动获取所有视频节目单
	jobWrapper.RegTask("vodTVTask", crawltask.CrawlVodTVTask.VodTVTask)
	// 【点播】填充剧相关信息
	jobWrapper.RegTask("vodTVPadInfoTask", crawltask.CrawlVodTVTask.VodTVPadInfoTask)
	// 【点播】填充剧ID
	jobWrapper.RegTask("vodTVPadIdTask", videoCrawlTask.CrawlVodTVTask.VodTVPadIdTask)
	// 【点播】填充集数ID
	jobWrapper.RegTask("vodTVItemPadIdTask", videoCrawlTask.CrawlVodTVTask.VodTVItemPadIdTask)

	// 点播默认爬虫处理
	jobWrapper.RegTask("crawlUrlTask", crawltask.CrawlTask.CrawlUrlTask)
	// 点播默认下载处理
	jobWrapper.RegTask("downloadMp4Task", crawltask.DownloadMp4Task)
	// 点播默认转码处理
	jobWrapper.RegTask("transformTask", crawltask.TransformTask)

	// 重置hosttype=2 解析失败（2）和下载失败（5）的记录
	jobWrapper.RegTask("resetHostType2Task", vodservice.ResetHostType2)

	// 重置等待下载（3）状态的记录
	jobWrapper.RegTask("resetProcessingTask", vodservice.ResetProcessingTooLong)
	// 重置正在爬取 (1)状态的记录
	jobWrapper.RegTask("resetCrawlingTask", vodservice.ResetCrawlingTooLong)
	// 查找转码完成没有post到cms成功的数据重新post
	jobWrapper.RegTask("checkPostCms", crawltask.PostCmsTask)

	// 回看 - 生成节目单
	jobWrapper.RegTask("initManifestTasks", replay.Manifest.InitManifestTasks)
	jobWrapper.RegTask("crawlReplayManifest", replay.Manifest.CrawlReplayManifest)

	// 录制
	jobWrapper.RegTask("currentRecordingTask", replay.Program.CreateAllCurrentRecordingTask) // 启动的时候录制当前进行中的
	jobWrapper.RegTask("futureRecordingTask", replay.Program.CreateAllFutureRecordingTask)   // 创建时间还未到后续需要录制的
	//自动删除N天之前的文件 每天凌晨一点执行
	jobWrapper.RegTask("deleCache", replay.Program.DeleCache)
	//自动删除N天之前的数据库沉淀 每天凌晨一点执行
	jobWrapper.RegTask("deleCacheSql", replay.Program.DeleCacheSql)
	//回看切片完成发布失败时再次检查发布
	registryTaskDiy()
	//转码发布
	//r := &replay.CrawlReplayProgram{}
	jobWrapper.RegTask("successRecord", replay.Program.CheckManifestTaskCrawlFinish)
	RegistryOnceTask("@every 5s", "fixRecordingTask", replay.Program.CreateAllFixRecordingTask)
	jobWrapper.RegTask("transformRecordedTask", replay.Transform.TransformTask)

	// 直播
	RegistryOnceTask("@every 5s", task.CrawlLiveTask, livetask.CrawlTask.Execute)

}

func RegistryOnceTask(pattern string, taskName string, job gcron.JobFunc) {
	taskfactory.RegistryOnceTask(pattern, taskName, job)
}

//排队任务
func registryTaskDiy() {
	taskNameList := configservice.GetStrings("server.openReplayTaskList")
	if taskNameList == nil {
		return
	}

	for _, Name := range taskNameList {
		//g.Dump(Name)
		if Name == "PostSuccessReplay" {
			for {
				replay.Program.PostSuccessReplay()
				time.Sleep(time.Millisecond * 300)
			}
		}

	}

}
