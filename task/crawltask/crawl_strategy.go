package crawltask

import (
	"github.com/gogf/gf/v2/text/gregex"
	"github.com/gogf/gf/v2/text/gstr"
	"uni-crawl-frame/core"
	"uni-crawl-frame/db/mysql/model/entity"
	"uni-crawl-frame/service/crawl/vodservice"
	"uni-crawl-video/task/vodtask/banan"
	"uni-crawl-video/task/vodtask/bilibili"
	"uni-crawl-video/task/vodtask/nivod"
	"uni-crawl-video/task/vodtask/nunuyy"
	"uni-crawl-video/task/vodtask/olevod"
)

const (
	Nunuyy     = "nunuyy"
	Bilibbili  = "bilibili"
	Ole        = "ole"
	TangRenJie = "tangrenjie.tv"
	QQ         = "v.qq.com"
	NiVod      = "nivod\\d*\\.tv"
	MudVod     = "mudvod\\d*\\.tv"
	Banan      = "banan.tv"
	Iqiyi      = "iqiyi.com"
)

type CrawlVideoStrategySelector struct {
}

func (r *CrawlVideoStrategySelector) GetCrawlVodFlowStrategy(seed *entity.CmsCrawlQueue) core.CrawlVodFlowInterface {
	return doGetCrawlVodFlowStrategy(seed.CrawlSeedUrl)
}

func (r *CrawlVideoStrategySelector) GetCrawlVodTVStrategy(seed *entity.CmsCrawlVodConfig) core.CrawlVodTVInterface {
	url := seed.SeedUrl

	if gstr.Contains(url, NiVod) || gstr.ContainsI(url, MudVod) {
		return new(nivod.NiVodTVTask)
	} else if gstr.Contains(url, Banan) {
		return new(banan.BananTvCrawl)
	}

	return nil
}

func (r *CrawlVideoStrategySelector) GetCrawlVodPadInfoStrategy(seed *entity.CmsCrawlVodTv) core.CrawlVodTVInterface {
	url := seed.SeedUrl
	if gstr.Contains(url, NiVod) || gstr.ContainsI(url, MudVod) {
		return new(nivod.NiVodTVTask)
	} else if gstr.Contains(url, Banan) {
		return new(banan.BananTvCrawl)
	}

	return nil
}

func (r *CrawlVideoStrategySelector) GetHostType(crawlSeedUrl string) int {
	if gstr.Contains(crawlSeedUrl, QQ) {
		return vodservice.HostTypeCrawlLogin
	} else if gstr.Contains(crawlSeedUrl, Iqiyi) {
		return vodservice.HostTypeCrawlLogin
	} else {
		return vodservice.HostTypeNormal
	}
}

func doGetCrawlVodFlowStrategy(url string) core.CrawlVodFlowInterface {
	if gstr.Contains(url, Nunuyy) {
		return new(nunuyy.NunuyyCrawl)
	} else if gstr.Contains(url, Bilibbili) {
		return new(bilibili.BilibiliCrawl)
	} else if gstr.Contains(url, Ole) {
		return new(olevod.OleVodCrawl)
	} else if gregex.IsMatchString(NiVod, url) || gregex.IsMatchString(MudVod, url) {
		return new(nivod.NiVodCrawl)
	}

	return nil
}
