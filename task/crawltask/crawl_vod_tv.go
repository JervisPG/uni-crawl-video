package crawltask

import (
	"context"
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/os/gctx"
	"github.com/xxl-job/xxl-job-executor-go"
	"uni-crawl-frame/service/crawl/vodservice"
)

var CrawlVodTVTask = new(crawlVodTVTask)

type crawlVodTVTask struct {
}

// 填充视频ID
func (crawlUrl *crawlVodTVTask) VodTVPadIdTask(context context.Context, param *xxl.RunReq) (msg string) {
	log := g.Log().Line()

	// 把填充视频基础信息的记录拿出来填充视频ID
	vodTV := vodservice.GetVodTvByStatus(vodservice.CrawlTVPadInfoOK)
	if vodTV == nil {
		return
	}
	vodservice.UpdateVodTVStatus(vodTV, vodservice.CrawlTVPadId)
	log.Infof(gctx.GetInitCtx(),
		"更新vod tv. id = %v, to status = %v", vodTV.Id, vodservice.CrawlTVPadIdOk)
	vodservice.UpdateVodTVStatus(vodTV, vodservice.CrawlTVPadIdOk)

	return
}

func (crawlUrl *crawlVodTVTask) VodTVItemPadIdTask(context context.Context, param *xxl.RunReq) (msg string) {
	log := g.Log().Line()

	vodTVItem := vodservice.GetPreparedVodTvItem()
	if vodTVItem == nil {
		return
	}
	vodTv := vodservice.GetVodTvById(vodTVItem.TvId)

	vodTVItem.VideoCollId = vodTv.VideoCollId // 修复异常情况
	log.Infof(gctx.GetInitCtx(),
		"update vod tv item. id = %v, status = %v", vodTVItem.Id, vodTVItem.CrawlStatus)
	vodservice.UpdateVodTVItemStatus(vodTVItem, vodservice.CrawlTVItemPadId)

	if vodTv == nil {
		return
	}
	log.Infof(gctx.GetInitCtx(), "addSubId:%v", vodTv.Id)
	if vodTv.Id == 0 || g.NewVar(vodTv.Id).IsEmpty() {
		return
	}

	vodservice.UpdateVodTVItemStatus(vodTVItem, vodservice.CrawlTVItemPadIdOk)
	vodservice.TransToCrawlQueue(vodTv, vodTVItem)

	return
}
