package crawltask

import (
	"github.com/gogf/gf/v2/os/gctx"
	"testing"
	"uni-crawl-frame/task/crawltask"
)

func TestDownloadMp4Task(t *testing.T) {
	crawltask.DownloadMp4Task(gctx.GetInitCtx())
}

func TestDownloadMp4Type1Task(t *testing.T) {
	crawltask.DownloadMp4Type1Task(gctx.GetInitCtx())
}
