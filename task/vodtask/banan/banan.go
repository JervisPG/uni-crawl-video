package banan

import (
	"github.com/gogf/gf/v2/util/gconv"
	"uni-crawl-frame/core"
	"uni-crawl-frame/db/mysql/model/entity"
	"uni-crawl-frame/service/crawl/vodservice"
	"uni-crawl-frame/utils/ffmpegutil"
)

type BananCrawl struct {
	*core.AbstractCrawlVodFlow
}

func (r *BananCrawl) UseBrowser() bool {
	return false
}

func (c *BananCrawl) FillTargetRequest(ctx *core.ApplicationContext) {

	vodTvItem := vodservice.GetVodTvItemByVideoItemId(gconv.String(ctx.CrawlQueueSeed.VideoItemId))
	if vodTvItem == nil {
		ctx.CrawlQueueSeed.CrawlM3U8Url = ""
	} else {
		// 直接使用抓详细信息时已经获取到的m3u8
		ctx.CrawlQueueSeed.CrawlM3U8Url = vodTvItem.SeedParams
	}
}

func (c *BananCrawl) ConvertM3U8(seed *entity.CmsCrawlQueue, filePath string) (*ffmpegutil.M3u8DO, error) {
	baseUrl := c.ConvertM3U8GetBaseUrl(seed.CrawlM3U8Url)
	return ffmpegutil.ConvertM3U8(seed.CrawlM3U8Url, baseUrl, filePath)
}
