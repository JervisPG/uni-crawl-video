package bilibili

type Data struct {
	Aid       int `json:"aid"`
	Cid       int `json:"cid"`
	VideoData struct {
		Title string `json:"title"`
		Pages []struct {
			Cid       int    `json:"cid"`
			Page      int    `json:"page"`
			Part      string `json:"part"`
			Duration  int    `json:"duration"`
			Dimension struct {
				Width  int `json:"width"`
				Height int `json:"height"`
			} `json:"dimension"`
		} `json:"pages"`
	} `json:"videoData"`
}

type Video struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
	Data    Result `json:"data"`
	Result  Result `json:"result"`
}

type Result struct {
	Quality    int    `json:"quality"`
	Format     string `json:"format"`
	Timelength int    `json:"timelength"` // ms
	Durl       []struct {
		URL    string `json:"url"`
		Order  int    `json:"order"`
		Length int    `json:"length"`
		Size   int    `json:"size"`
	} `json:"durl"`
	SupportFormats []struct {
		Quality        int    `json:"quality"`
		Format         string `json:"format"`
		NewDescription string `json:"new_description"`
	} `json:"support_formats"`
}
