package bilibili

import (
	"uni-crawl-frame/core"
)

type BilibiliVIPCrawl struct {
	*core.AbstractCrawlVodFlow
}

func (r *BilibiliVIPCrawl) OpenBrowser(ctx *core.ApplicationContext) {
	_ = ctx.Wd.Get(ctx.CrawlQueueSeed.CrawlSeedUrl)
}

func (r *BilibiliVIPCrawl) FillTargetRequest(ctx *core.ApplicationContext) {
	// 学习使用，请误非法用途
}
