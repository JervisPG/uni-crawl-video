package nivod

import (
	"fmt"
	"github.com/gogf/gf/v2/text/gregex"
	"testing"
)

func TestImg(t *testing.T) {
	stype := "height: inherit; width: inherit; background-size: 100%; animation-duration: 0.4s; animation-delay: 0.0726115s; background-image: url(https://static.mudvod.tv/imgs/2022/11/12/c96bd0fc-f026-483e-a32d-7e12f37725e1.jpg_300x400.jpg);"
	matchString, _ := gregex.MatchString("http.*jpg", stype)
	fmt.Println(matchString)
}
