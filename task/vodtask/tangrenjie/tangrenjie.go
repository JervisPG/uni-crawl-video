package tangrenjie

import (
	"github.com/gogf/gf/v2/os/gtime"
	"uni-crawl-frame/core"
	"uni-crawl-frame/db/mysql/model/entity"
	"uni-crawl-frame/utils/browsermob"
	"uni-crawl-frame/utils/browserutil"
	"uni-crawl-frame/utils/ffmpegutil"
)

var (
	videoXpath = "//*[@id='player']"
)

type TangRenJieCrawl struct {
	*core.AbstractCrawlVodFlow
}

func (c *TangRenJieCrawl) OpenBrowser(ctx *core.ApplicationContext) {
	_ = ctx.Wd.WaitWithTimeout(browserutil.GetXpathCondition(videoXpath), gtime.S*30)
	_ = ctx.Wd.Get(ctx.CrawlQueueSeed.CrawlSeedUrl)

	// 唐人街跟欧乐一样html有player_aaaa元素，欧乐里player_aaaa json可以直接拿到url，但唐人街对url进行加密了，如：
	// "url": "JTY4JTc0JTc0JTcwJTczJTNBJTJGJTJGJTZEJTMzJTc1JTJFJTY5JTY2JTMxJTMwJTMxJTJFJTc0JTc2JTJGJTc4JTZEJTMzJTc1JTM4JTJGJTM2JTM4JTMxJTM2JTMyJTMzJTM4JTY1JTY0JTYxJTY1JTYyJTM1JTYyJTMwJTYyJTM2JTM4JTM5JTM3JTM3JTM3JTYyJTY2JTY1JTM0JTYxJTM1JTM4JTMxJTM2JTM4JTY0JTYzJTMzJTM3JTY1JTYyJTYyJTMyJTYxJTM5JTMwJTY2JTY1JTY0JTMwJTY2JTY2JTM3JTY2JTY0JTYyJTMwJTY0JTM0JTM0JTM1JTY2JTY2JTMwJTYzJTYzJTM1JTM5JTM5JTMyJTMxJTY2JTMxJTMxJTY1JTM5JTM3JTY0JTMwJTY0JTYxJTMyJTMxJTJFJTZEJTMzJTc1JTM4",
	// 因此需要直接用mob代理去获取m3u8
}

func (c *TangRenJieCrawl) FillTargetRequest(ctx *core.ApplicationContext) {
	request := browsermob.GetHarRequestLocalRetry(ctx.XClient, "hls/index.m3u8", "")
	if request == nil {
		request = browsermob.GetHarRequestLocalRetry(ctx.XClient, "\\.m3u8", "")
	}
	if request != nil {
		ctx.CrawlQueueSeed.CrawlM3U8Url = request.Get("url").String()
	}
}

func (c *TangRenJieCrawl) ConvertM3U8(seed *entity.CmsCrawlQueue, filePath string) (*ffmpegutil.M3u8DO, error) {
	m3u8DO, err := c.AbstractCrawlVodFlow.ConvertM3U8(seed, filePath)
	m3u8DO.PngHeaderSize = 308
	return m3u8DO, err
}
