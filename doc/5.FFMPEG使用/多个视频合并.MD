### 方法0: 左右拼接版
```shell
ffmpeg -i 1.mp4 -i 2.mp4 -filter_complex hstack output.mp4
```

### 方法一: 参数不太直观
```shell
ffmpeg -i 1.mp4 -i 2.mp4 -filter_complex "[0:v] [0:a] [1:v] [1:a] concat=n=2:v=1:a=1 [v] [a]" -map "[v]" -map "[a]" output.mp4
```

### 方法二: 建议使用
- 新建filelist.txt，内容如下
```text
file '11.mp4'
file '22.mp4'
```
- 执行命令
```shell
ffmpeg -f concat -safe 0 -i filelist.txt -y output.mp4
```
