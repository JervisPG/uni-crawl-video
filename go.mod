module uni-crawl-video

go 1.18

require (
	github.com/antchfx/htmlquery v1.3.0
	github.com/gocolly/colly v1.2.0
	github.com/gogf/gf/contrib/config/nacos/v2 v2.6.3 // indirect
	github.com/gogf/gf/contrib/drivers/mysql/v2 v2.6.3 // indirect
	github.com/gogf/gf/v2 v2.6.3
	github.com/gogf/swagger/v2 v2.0.0 // indirect
	github.com/tebeka/selenium v0.9.9
	github.com/xxl-job/xxl-job-executor-go v1.2.0
	uni-crawl-frame v1.0.0
)

// 改为本地引用，方便直接改frame层代码
replace uni-crawl-frame v1.0.0 => ../uni-crawl-frame

require (
	github.com/aliyun/alibaba-cloud-sdk-go v1.62.680 // indirect
	github.com/buger/jsonparser v1.1.1 // indirect
	github.com/go-basic/ipv4 v1.0.0 // indirect
	github.com/go-errors/errors v1.5.1 // indirect
	github.com/go-sql-driver/mysql v1.7.1 // indirect
	github.com/jmespath/go-jmespath v0.4.0 // indirect
	github.com/json-iterator/go v1.1.12 // indirect
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/modern-go/reflect2 v1.0.2 // indirect
	github.com/nacos-group/nacos-sdk-go v1.1.4 // indirect
	github.com/opentracing/opentracing-go v1.2.1-0.20220228012449-10b1cf09e00b // indirect
	github.com/pkg/errors v0.9.1 // indirect
	go.opentelemetry.io/otel/metric v1.24.0 // indirect
	go.uber.org/multierr v1.11.0 // indirect
	go.uber.org/zap v1.27.0 // indirect
	golang.org/x/sync v0.6.0 // indirect
	gopkg.in/ini.v1 v1.67.0 // indirect
	gopkg.in/natefinch/lumberjack.v2 v2.2.1 // indirect
)

require (
	github.com/BurntSushi/toml v1.3.2 // indirect
	github.com/PuerkitoBio/goquery v1.8.0
	github.com/andybalholm/cascadia v1.3.1 // indirect
	github.com/antchfx/xmlquery v1.3.12 // indirect
	github.com/antchfx/xpath v1.2.3 // indirect
	github.com/bitly/go-simplejson v0.5.0 // indirect
	github.com/blang/semver v3.5.1+incompatible // indirect
	github.com/clbanning/mxj/v2 v2.7.0 // indirect
	github.com/corpix/uarand v0.2.0 // indirect
	github.com/fatih/color v1.16.0 // indirect
	github.com/fsnotify/fsnotify v1.7.0 // indirect
	github.com/go-logr/logr v1.4.1 // indirect
	github.com/go-logr/stdr v1.2.2 // indirect
	github.com/gobwas/glob v0.2.3 // indirect
	github.com/golang/groupcache v0.0.0-20210331224755-41bb18bfe9da // indirect
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/gorilla/websocket v1.5.1 // indirect
	github.com/grokify/html-strip-tags-go v0.1.0 // indirect
	github.com/kennygrant/sanitize v1.2.4 // indirect
	github.com/kr/pretty v0.3.1 // indirect
	github.com/magiconair/properties v1.8.7 // indirect
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.20 // indirect
	github.com/mattn/go-runewidth v0.0.15 // indirect
	github.com/olekukonko/tablewriter v0.0.5 // indirect
	github.com/rivo/uniseg v0.4.7 // indirect
	github.com/saintfish/chardet v0.0.0-20120816061221-3af4cd4741ca // indirect
	github.com/temoto/robotstxt v1.1.2 // indirect
	go.opentelemetry.io/otel v1.24.0 // indirect
	go.opentelemetry.io/otel/sdk v1.24.0 // indirect
	go.opentelemetry.io/otel/trace v1.24.0 // indirect
	golang.org/x/net v0.21.0 // indirect
	golang.org/x/sys v0.17.0 // indirect
	golang.org/x/text v0.14.0 // indirect
	google.golang.org/appengine v1.6.1 // indirect
	google.golang.org/protobuf v1.26.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
