package main

import (
	"github.com/gogf/gf/v2/frame/g"
	"uni-crawl-frame/core"
	_ "uni-crawl-frame/middleware/mysql/boot"
	_ "uni-crawl-frame/middleware/nacos/boot"
	_ "uni-crawl-frame/router"
	"uni-crawl-frame/task/xxljob"
	"uni-crawl-video/task"
	videocrawltask "uni-crawl-video/task/crawltask"
)

func init() {
	core.StrategySelector = new(videocrawltask.CrawlVideoStrategySelector)
}

func main() {
	// xxl-job
	jobWrapper := new(xxljob.XxlJobWrapper)
	jobWrapper.Init()

	task.RegistryAllDefaultVideoTask(jobWrapper)
	//task.StartAll()

	jobWrapper.Run()

	g.Server().Run()
}
